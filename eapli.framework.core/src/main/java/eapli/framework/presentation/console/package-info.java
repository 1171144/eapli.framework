/**
 * simple presentation framework using the console
 *
 * @author Paulo Gandra Sousa
 *
 */
package eapli.framework.presentation.console;

/*
 *

@startuml
interface Controller
class AbstractUI
Controller <.. AbstractUI
class Menu
AbstractUI <|-- Menu
class MenuItem
MenuItem <-- Menu
interface Action
Action <-- MenuItem
class MenuRenderer
Menu <.. MenuRenderer
@enduml

 *
 *
 */