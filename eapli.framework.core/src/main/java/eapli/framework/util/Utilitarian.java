/**
 *
 */
package eapli.framework.util;

/**
 * Marker interface for utilitarian classes. Typically it will be a final class
 * with just static methods
 *
 * @author sou03408
 *
 */
public interface Utilitarian {

}
