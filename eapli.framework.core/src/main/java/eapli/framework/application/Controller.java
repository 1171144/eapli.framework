/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package eapli.framework.application;

/**
 * A marker interface for use case controllers. A Use case controller is the API to the domain translating from the outside world (e.g., DTO) to the inside world (domain).
 * use case controllers do not perform any business logic but coordinate/dispatch the execution to the right business object
 *
 * @author Paulo Gandra Sousa
 */
public interface Controller {
}
